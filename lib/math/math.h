#ifndef MATH_H
#define MATH_H

#include <math.h>

float fast_sin(int32_t in) // fast sin, it's just a LUT
{
    float out[91] = { 0.000000, 0.017452, 0.034899, 0.052336, 0.069756, 0.087156, 0.104528,
                      0.121869, 0.139173, 0.156434, 0.173648, 0.190809, 0.207912, 0.224951,
                      0.241922, 0.258819, 0.275637, 0.292372, 0.309017, 0.325568, 0.342020,
                      0.358368, 0.374606, 0.390731, 0.406736, 0.422618, 0.438371, 0.453990,
                      0.469471, 0.484809, 0.500000, 0.515038, 0.529919, 0.544639, 0.559193,
                      0.573576, 0.587785, 0.601815, 0.615662, 0.629320, 0.642788, 0.656059,
                      0.669131, 0.681998, 0.694658, 0.707107, 0.719340, 0.731354, 0.743145,
                      0.754710, 0.766045, 0.777146, 0.788011, 0.798636, 0.809017, 0.819152,
                      0.829038, 0.838671, 0.848048, 0.857167, 0.866026, 0.874620, 0.882948,
                      0.891007, 0.898794, 0.906308, 0.913546, 0.920505, 0.927184, 0.933581,
                      0.939693, 0.945519, 0.951057, 0.956305, 0.961262, 0.965926, 0.970296,
                      0.974370, 0.978148, 0.981627, 0.984808, 0.987688, 0.990268, 0.992546,
                      0.994522, 0.996195, 0.997564, 0.998630, 0.999391, 0.999848, 1.000000 };

    while (in >= 360) in -= 360; // sin is a periodic function
    while (in < 0) in += 360;

    if (in < 360 && in >= 270)
    {
        in = 360 - in;
        return -out[in];
    }
    else
    if (in < 270 && in >= 180)
    {
        in -= 180;
        return -out[in];
    }
    else
    if (in < 180 && in > 90)
    {
        in = 180 - in;
        return out[in];
    }
    else
    {
        return out[in];
    }

}
  //PostCondition: the return value would only be in the first ad forth quadrants
  // the caller can add 180 to get the value for corresponding quadrant 
float fast_atan(float in)
{
  float out[91] = {0.000000,   0.017455,   0.034921,   0.052408,   0.069927,   0.087489,   0.105104,   0.122785,   0.140541,   0.158384,  
                   0.176327,   0.194380,   0.212557,   0.230868,   0.249328,   0.267949,   0.286745,   0.305731,   0.324920,   0.344328,  
                   0.363970,   0.383864,   0.404026,   0.424475,   0.445229,   0.466308,   0.487733,   0.509525,   0.531709,   0.554309,  
                   0.577350,   0.600861,   0.624869,   0.649408,   0.674509,   0.700208,   0.726543,   0.753554,   0.781286,   0.809784,  
                   0.839100,   0.869287,   0.900404,   0.932515,   0.965689,   1.000000,   1.035530,   1.072369,   1.110613,   1.150368,  
                   1.191754,   1.234897,   1.279942,   1.327045,   1.376382,   1.428148,   1.482561,   1.539865,   1.600335,   1.664279,  
                   1.732051,   1.804048,   1.880726,   1.962611,   2.050304,   2.144507,   2.246037,   2.355852,   2.475087,   2.605089,  
                   2.747477,   2.904211,   3.077684,   3.270853,   3.487414,   3.732051,   4.010781,   4.331476,   4.704630,   5.144554,  
                   5.671282,   6.313752,   7.115370,   8.144346,   9.514364,   11.430052,  14.300666,  19.081137,  28.636253,  57.289962,  
                   230 }; // 230 instead of infinity for tan( > 89.5)
  float tan_val = in;
  if (in < 0){
    tan_val = -in; 
  }
  // from here to the next mark can be moved to another helper function *********
  // binary search to find the index
  uint32_t temp = 0 ;
  uint32_t low = 0;
  uint32_t high = 90; 
  while(low+1 < high){
    temp = (low + high)/2;
    if (tan_val < out[temp]){
      high = temp;
    } else {
      low  = temp; 
    }
  }

  // to make it a bit more accurate
  float mid_val = (out[low] + out [high]) / 2; 
  float ret = low; 
  if (tan_val > mid_val){
    ret = high; 
  }
// ******************* up to here can be moved to a helper func *****************

  if (in < 0 && ret != 0){
    return -ret; 
  } else {
    return ret ;
  }
  
}
// found here: https://www.codeproject.com/Articles/69941/Best-Square-Root-Method-Algorithm-Function-Precisi
// this is sqrt3() in the above webpage
float fast_sqrt(float x)
{
  union
  {
    int i;
    float x;
  } u;

  u.x = x;
  u.i = (1<<29) + (u.i >> 1) - (1<<22); 
  return u.x;
}

uint8_t float_to_uint8_t(float x)
{
  return (x >= 0) ? x : -x;
}

uint16_t float_to_uint16_t(float x)
{
  return (x >= 0) ? x : -x;
}

int8_t _sign(int32_t x)
{
    return (x >= 0) ? 1 : -1;
}

uint32_t _abs(int32_t x)
{
    return (x > 0) ? x : -x;
}

#endif
