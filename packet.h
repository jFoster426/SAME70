struct __attribute__((packed)) packetFormat {
    char robotId;
    float velocityTangent;
    float velocityNormal;
    float velocityAngular;
    float kickSpeedX;
    float kickSpeedZ;
    float spinner;
};



// Code from other places - for refrence
/*/
 * byte  0:     robotId
 * bytes 1-4:   velTangent  (as a single-precision floating point)
 * bytes 5-8:   velNormal   (as a single-precision floating point)
 * bytes 9-12:  velAngular  (as a single-precision floating point)
 * bytes 13-16: kickSpeedX  (as a single-precision floating point)
 * bytes 17-20: kickSpeedZ  (as a single-precision floating point)
 * bytes 21-24: spinner     (as a single-precision floating point)
 * byte  25:    a single newline character
 * byte  26:    a single null-terminating character
 * @return this RobotCommand represented as a packet
 * 
 * 
 * 
self._raw_packet = struct.pack("<cfffff?",
                                       bytes([command.id]), command.veltangent, command.velnormal,
                                       command.velangular, command.kickspeedx, command.kickspeedz,
                                       command.spinner)


*/